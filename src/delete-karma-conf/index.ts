import { Rule, SchematicContext, Tree } from "@angular-devkit/schematics";

export function deleteKarmaConf(options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const directory = `./${options.name}/karma.conf`;

    tree.delete(directory);
    return tree;
  };
}
