import {
  chain,
  Rule,
  schematic,
  SchematicContext,
  Tree,
} from "@angular-devkit/schematics";
import { IAngularStater } from "./schema";

export function angularStarter(options: IAngularStater): Rule {
  return (_tree: Tree, _context: SchematicContext) => {
    // Configuration
    const addPrettierConfiguration = schematic(
      "add-prettier-configuration",
      options
    );
    const addBrowserslistrcConfiguration = schematic(
      "add-browserslistrc",
      options
    );

    const addEslintJson = schematic("add-eslint-json", options);
    const addGitAttributes = schematic("add-git-attributes", options);
    const addGitIgnore = schematic("add-git-ignore", options);
    const addStyleLintrc = schematic("add-style-lintrc", options);
    const addChangeLog = schematic("add-change-log", options);
    const addEcosystem = schematic("add-ecosystem", options);
    const addJenkins = schematic("add-jenkins", options);
    const addNginx = schematic("add-nginx-conf", options);
    const addReadme = schematic("add-readme", options);
    const addServe = schematic("add-serve-json", options);
    const addTsConfig = schematic("add-tsconfig", options);
    const addHusky = schematic("add-husky", options);
    const addAngularJson = schematic("add-angular-json", options);
    const addDockerFile = schematic("add-docker-file", options);

    //other
    const addStylesFolder = schematic("add-styles-folder", options);
    const addPackageJson = schematic("add-package-json", options);
    const addApi = schematic("add-api", options);
    const addProxy = schematic("add-proxy", options);
    const addAppFolder = schematic("add-app-folder", options);

    return chain([
      addPrettierConfiguration,
      addBrowserslistrcConfiguration,
      addEslintJson,
      addGitAttributes,
      addGitIgnore,
      addStyleLintrc,
      addChangeLog,
      addEcosystem,
      addJenkins,
      addNginx,
      addReadme,
      addServe,
      addTsConfig,
      addHusky,
      addAngularJson,
      addDockerFile,
      addStylesFolder,
      addPackageJson,
      addApi,
      addProxy,
      addAppFolder,
    ]);
  };
}
