import {
  apply,
  move,
  Rule,
  SchematicContext,
  Tree,
  chain,
  mergeWith,
  url,
  MergeStrategy,
} from "@angular-devkit/schematics";
import { normalize } from "path";
import { PrettierSchema } from "./schema";

export function addPrettierConfiguration(options: PrettierSchema): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    const directory = `./${options.name}/`;

    const templateSource = apply(url("./files"), [move(normalize(directory))]);

    return chain([mergeWith(templateSource, MergeStrategy.Overwrite)]);
  };
}
