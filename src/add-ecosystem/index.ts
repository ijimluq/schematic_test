import { normalize } from "@angular-devkit/core";
import {
  apply,
  chain,
  MergeStrategy,
  mergeWith,
  move,
  Rule,
  SchematicContext,
  Tree,
  url,
} from "@angular-devkit/schematics";

export function addEcosystem(options: any): Rule {
  return (_tree: Tree, _context: SchematicContext) => {
    const directory = `./${options.name}`;
    const templateSource = apply(url("./files"), [move(normalize(directory))]);

    return chain([mergeWith(templateSource, MergeStrategy.Overwrite)]);
  };
}
