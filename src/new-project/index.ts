import {
  apply,
  chain,
  empty,
  externalSchematic,
  mergeWith,
  Rule,
  schematic,
  SchematicContext,
  Tree,
} from "@angular-devkit/schematics";

import {
  Schema as NgNewOptions,
  Style,
} from "@schematics/angular/ng-new/schema";

export function newProject(options: NgNewOptions): Rule {
  return async (_tree: Tree, _context: SchematicContext) => {
    options.style = Style.Scss;

    const ngNewOriginal = externalSchematic(
      "@schematics/angular",
      "ng-new",
      options
    );
    const customStarter = schematic("angular-starter", options);

    return chain([mergeWith(apply(empty(), [ngNewOriginal, customStarter]))]);
  };
}
