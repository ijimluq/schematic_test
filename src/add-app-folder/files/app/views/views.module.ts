import { NgModule } from '@angular/core';
import { ViewsRoutingModule } from './views-routing.module';

import { PageNotFoundModule } from './page-not-found/page-not-found.module';
import { LandingModule } from './landing/landing.module';
import { DemoModule } from './demo/demo.module';

@NgModule({
  declarations: [],
  imports: [ViewsRoutingModule, PageNotFoundModule, LandingModule, DemoModule]
})
export class ViewsModule {}
