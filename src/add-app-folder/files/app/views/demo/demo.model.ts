interface DemoBook {
  title: string;
  author: string;
}

export type DemoResponse = DemoBook[];
