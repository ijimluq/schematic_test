import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/core/api.service';
import { DemoResponse } from './demo.model';

@Injectable({
  providedIn: 'root'
})
export class DemoService {
  constructor(private http: HttpClient, private api: ApiService) {}

  getBooks(): Observable<DemoResponse> {
    return this.http.get<DemoResponse>(this.api.url.demo());
  }
}
