import { Component, OnInit } from '@angular/core';
import { DemoService } from './demo.service';
import { DemoResponse } from './demo.model';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  data: DemoResponse;
  isCollapsed = true;

  constructor(private demo: DemoService) {}

  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this.demo.getBooks().subscribe(data => {
      this.data = data;
    });
  }
}
