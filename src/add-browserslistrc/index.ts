import { normalize } from "@angular-devkit/core";
import {
  apply,
  chain,
  forEach,
  MergeStrategy,
  mergeWith,
  move,
  Rule,
  SchematicContext,
  Source,
  Tree,
  url,
} from "@angular-devkit/schematics";

export function applyWithOverwrite(source: Source, rules: Rule[]): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const rule = mergeWith(
      apply(source, [
        ...rules,
        forEach((fileEntry) => {
          if (tree.exists(fileEntry.path)) {
            tree.overwrite(fileEntry.path, fileEntry.content);
            return null;
          }
          return fileEntry;
        }),
      ])
    );

    return rule(tree, _context);
  };
}

export function addBrowserslistrc(options: any): Rule {
  return (_tree: Tree, _context: SchematicContext) => {
    const directory = `./${options.name}`;

    const templateSource = apply(url("./files"), [move(normalize(directory))]);

    return chain([mergeWith(templateSource, MergeStrategy.Overwrite)]);
  };
}
